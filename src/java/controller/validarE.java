package controller;


import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("validarE")
public class validarE implements Validator{

    private static final String Nombre_valid = "^[a-zA-Z]+[a-zA-Z0-9._-]*@[a-zA-Z0-9]+.[a-z]{2,3}$";
    
    
    private final Pattern pattern;
    private Matcher matcher;
 
    
    public validarE(){
		  pattern = Pattern.compile(Nombre_valid);
	}
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String Nombre=(String)value;
        
        matcher = pattern.matcher(value.toString());
		if(!matcher.matches()){
 
			FacesMessage msg= new FacesMessage("El email deve tener la forma ejemplo@email.com");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
 
        
                }
    }
    
    
    
    
}
