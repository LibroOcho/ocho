/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contoller;


import controller.carrito;
import controller.libcant;
import entidad.Autor;
import entidad.Autorlibro;
import entidad.Categoria;
import entidad.Cliente;
import entidad.Detallepedido;
import entidad.Libro;

import entidad.Pedido;
import entidad.Usuario;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.swing.text.Document;
import javax.validation.constraints.Future;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.primefaces.model.UploadedFile;
import session.AutorFacade;
import session.AutorlibroFacade;
import session.CategoriaFacade;
import session.ClienteFacade;
import session.DetallepedidoFacade;
import session.LibroFacade;
import session.PedidoFacade;
import session.UsuarioFacade;

/**
 *
 * @author DellPC
 */
@ManagedBean
@SessionScoped
public class Libreria implements Serializable {

//--Inyeccion de dependencias---
    @EJB
    private AutorFacade autorfac;
    @EJB
    private AutorlibroFacade autorlibfac;
    @EJB
    private CategoriaFacade categoriafac;
    @EJB
    private ClienteFacade clientefac;
    @EJB
    private DetallepedidoFacade detallepedidofac;
    @EJB
    private LibroFacade librofac;
    @EJB
    private PedidoFacade pedidofac;

    @EJB
    private UsuarioFacade usuariofac;

    /**
     * Creates a new instance of Libreria
     */
    ///----Clasess Entidad///////////////////77
    private Autor autor;
    private Autorlibro autlibro;
    private Categoria categoria;
    private Cliente cliente;
    private Detallepedido detallepe;
    private Libro libro;
    private Pedido pedido;

    private Usuario usuario;
    private Libro libroalta;

    Date hoy = new Date();
    ///////////////////////////////////////////

    /////// Listas de Entidad/////////////////
    private List<Libro> libroList;
    private List<Pedido> pedidoList;
    private List<Autorlibro> autorlibroList;
    private List<Detallepedido> detallepedidoList;
    private List<Categoria> categoriaList;

    private List<Autor> autorList;
    private List<Cliente> clienteList;
    ////////////////////////////////////////
    ////////////login y busqueda variables//////////////7
    @NotNull
    @Size(min = 6, max = 20, message = "login muy corto")
    private String login;
    @NotNull
    @Size(min = 6, max = 20, message = "password no debe ser menor de 8 caracteres")
    private String pwd;
    private String busqueda = "";
    private Timestamp Fecha;
    private String nombre;
    private String isbn;
    private String titulo;
    private int existencias;
    private Double precio;
    private String descripcion;

    private int cantidad;
    private String password;

    private int idAutor;
    private int cantidadpago;
    private String opciones;
    private String nombres;
    private String apellidos;
    private String email;
    private String telefono;
    private String Tarjeta;
    @Future
    private Date FechaV;

    private Integer idCategoria;

    /////////////////////////////////////////////////////
    public Libreria() {
    }
////////////////get y set facades///////////////////////77777777

    public AutorFacade getAutorfac() {
        return autorfac;
    }

    public void setAutorfac(AutorFacade autorfac) {
        this.autorfac = autorfac;
    }

    public AutorlibroFacade getAutorlibfac() {
        return autorlibfac;
    }

    public void setAutorlibfac(AutorlibroFacade autorlibfac) {
        this.autorlibfac = autorlibfac;
    }

    public CategoriaFacade getCategoriafac() {
        return categoriafac;
    }

    public void setCategoriafac(CategoriaFacade categoriafac) {
        this.categoriafac = categoriafac;
    }

    public ClienteFacade getClientefac() {
        return clientefac;
    }

    public void setClientefac(ClienteFacade clientefac) {
        this.clientefac = clientefac;
    }

    public DetallepedidoFacade getDetallepedidofac() {
        return detallepedidofac;
    }

    public void setDetallepedidofac(DetallepedidoFacade detallepedidofac) {
        this.detallepedidofac = detallepedidofac;
    }

    public LibroFacade getLibrofac() {
        return librofac;
    }

    public void setLibrofac(LibroFacade librofac) {
        this.librofac = librofac;
    }

    public PedidoFacade getPedidofac() {
        return pedidofac;
    }

    public void setPedidofac(PedidoFacade pedidofac) {
        this.pedidofac = pedidofac;
    }

    public UsuarioFacade getUsuariofac() {
        return usuariofac;
    }

    public void setUsuariofac(UsuarioFacade usuariofac) {
        this.usuariofac = usuariofac;
    }
////////////////////get y set entidades//////////////////////////////////

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Autorlibro getAutlibro() {

        return autlibro;
    }

    public void setAutlibro(Autorlibro autlibro) {
        this.autlibro = autlibro;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Detallepedido getDetallepe() {
        return detallepe;
    }

    public void setDetallepe(Detallepedido detallepe) {
        this.detallepe = detallepe;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

////////////get y set listas///////////////////77
    public List<Libro> getLibroList() {
        // return this.librofac.findAll();
        return libroList;
    }

    public void setLibroList(List<Libro> libroList) {
        this.libroList = libroList;
    }

    public List<Pedido> getPedidoList() {
        return pedidoList;
    }

    public void setPedidoList(List<Pedido> pedidoList) {
        this.pedidoList = pedidoList;
    }

    public List<Autorlibro> getAutorlibroList() {
        autorlibroList = autorlibfac.findAll();
        return autorlibroList;
    }

    public void setAutorlibroList(List<Autorlibro> autorlibroList) {
        this.autorlibroList = autorlibroList;
    }

    public List<Detallepedido> getDetallepedidoList() {
        return detallepedidoList;
    }

    public void setDetallepedidoList(List<Detallepedido> detallepedidoList) {
        this.detallepedidoList = detallepedidoList;
    }

    public List<Categoria> getCategoriaList() {
        categoriaList = categoriafac.findAll();
        return categoriaList;
    }

    public void setCategoriaList(List<Categoria> categoriaList) {
        this.categoriaList = categoriaList;
    }

///////////////////////////////get y set variables///////////////////
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public Timestamp getFecha() {
        return Fecha;
    }

    public void setFecha(Timestamp Fecha) {
        this.Fecha = Fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getExistencias() {
        return existencias;
    }

    public void setExistencias(int existencias) {
        this.existencias = existencias;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getOpciones() {
        return opciones;
    }

    public void setOpciones(String opciones) {
        this.opciones = opciones;
    }

    public Date getHoy() {
        return hoy;
    }

    public void setHoy(Date hoy) {
        this.hoy = hoy;
    }

    //////////////////metodoss///////////77
    public String busca() {
        switch (opciones) {
            case "autor":
                busquedaAutor();
                break;
            case "titulo":
                busquedaTitulo();
                break;
            case "tema":
                busquedaCategoria();
                break;
            case "ISBN":
                busquedaIsbn();
                break;
        }
        return "catalogo";
    }

    public String busquedaIsbn() {
        String pagina = "";
        libroList = new ArrayList();
        for (Libro lib : librofac.findAll()) {
            if (lib.getIsbn().equals(busqueda)) {
                libroList.add(lib);
                return "PageIsbn";
            }
        }
        return "";

    }

    public String busquedaCategoria() {
        libroList = new ArrayList();
        for (Libro lib : librofac.findAll()) {
            if (lib.getIdCategoria().getNombre().equals(busqueda)) {
                libroList.add(lib);
            }
        }
        return "catalogo";
    }

    public String busquedaAutor() {
        libroList = new ArrayList();
        for (Libro lib : librofac.findAll()) {
            autorlibroList = lib.getAutorlibroList();
            for (Autorlibro autlib : autorlibroList) {
                autor = autorfac.find(autlib.getIdAutor().getIdAutor());
                if (autor.getNombres().equals(busqueda)) {
                    libroList.add(lib);
                }
            }
        }
        return "catalogo";
    }

    public String busquedaTitulo() {
        libroList = new ArrayList();
        for (Libro lib : librofac.findAll()) {
            if (lib.getTitulo().equals(busqueda)) {
                libroList.add(lib);
            }
        }
        return "catalogo";

    }

    public String loguea() {
        usuario = null;
        String pagina = "";
        List<Usuario> usuarios = usuariofac.findAll();
        for (Usuario usu : usuarios) {
            if (usu.getLogin().equals(login) && usu.getPwd().equals(pwd)) {
                usuario = usu;
            }
        }
        if (usuario != null) {
            if (usuario.getPuesto().equals("Administrador")) {
                return "administrador";
            } else {
                return "usuario";
            }
        } else {
            JsfUtil.addErrorMessage("No estas registrado intruso");
            return "";
        }

    }

    //operaciones crud///
    public String altaLibro() {
        Libro l = new Libro();
        l.setIsbn(isbn);
        l.setTitulo(titulo);
        l.setExistencias(existencias);
        l.setPrecio(precio);
        l.setDescripcion(descripcion);
        l.setIdCategoria(categoriafac.find(idCategoria));
        librofac.create(l);
        return "Altalibro";

    }

    public String altaCategoria() {
        Categoria c = new Categoria();
        c.setNombre(nombre);
        categoriafac.create(c);
        return "";

    }

    public List<Autor> getAutorList() {
        autorList = autorfac.findAll();
        return autorList;
    }

    public void setAutorList(List<Autor> autorList) {
        this.autorList = autorList;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getCantidadpago() {
        return cantidadpago;
    }

    public void setCantidadpago(int cantidadpago) {
        this.cantidadpago = cantidadpago;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String upload() {
        if (file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        }
        return "";
    }

    public String irCarrito(Libro lib) {
        libro = lib;
        return "carro";
    }

    public String logueaCliente() {
        cliente = null;
        String pagina = "";
        List<Cliente> clienteList = clientefac.findAll();
        for (Cliente cli : clienteList) {
            if (cli.getLogin().equals(login) && cli.getPassword().equals(password)) {
                cliente = cli;
                return "pagoApartado";
            }
        }
        return "";
    }

    public String addCliente() {
        Cliente c = new Cliente();
        c.setNombres(nombres);
        c.setApellidos(apellidos);
        c.setTelefono(telefono);
        c.setEmail(email);
        c.setLogin(login);
        c.setPassword(password);
        clientefac.create(c);
        return "pagoApartado";

    }

    public String editCliente() {
        Cliente c = cliente;
        c.setNombres(nombres);
        c.setApellidos(apellidos);
        c.setTelefono(telefono);
        c.setEmail(email);
        c.setLogin(login);
        c.setPassword(password);
        clientefac.create(c);
        cliente = c;
        return "pagoApartado";

    }

    public Libro getLibroalta() {
        return libroalta;
    }

    public void setLibroalta(Libro libroalta) {
        this.libroalta = libroalta;
    }

    public String altaAutor() {
        Autor a = new Autor();
        a.setNombres(nombres);
        a.setApellidos(apellidos);
        autorfac.create(a);
        return "";
    }

    public String altaAutorLibro() {
        Autorlibro al = new Autorlibro();
        al.setIdLibro(librofac.find(isbn));
        al.setIdAutor(autorfac.find(idAutor));
        autorlibfac.create(al);
        return "";
    }

    public int getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(int idAutor) {
        this.idAutor = idAutor;
    }

    public String editAutorLibro() {
        Autorlibro al = new Autorlibro();
        autorlibfac.edit(al);
        return "";
    }

    public String editAutor() {
        Autor a = new Autor();
        a.setNombres(nombres);
        a.setApellidos(apellidos);
        autorfac.edit(a);
        return "";
    }

    public String editLibro() {
        Libro l = new Libro();
        l.setIsbn(isbn);
        l.setTitulo(titulo);
        l.setExistencias(existencias);
        l.setPrecio(precio);
        l.setDescripcion(descripcion);
        l.setIdCategoria(categoriafac.find(idCategoria));
        librofac.edit(l);
        return "Altalibro";

    }

    public String editCategoria() {
        Categoria c = categoriafac.find(idCategoria);
        c.setNombre(nombre);
        categoriafac.create(c);
        return "Editcategoria";

    }

    public String vender(carrito car) {
        double subT = 0.0;
        double dep = 0;
        List<libcant> list = car.getCarro();
        for (libcant li : list) {
            subT += (li.getLibro().getPrecio() * li.getCantidad());
            libro = li.getLibro();
            libro.setExistencias(libro.getExistencias() - li.getCantidad());
            librofac.edit(libro);
        }
        dep = (subT * 1.16);
        pedido = new Pedido();
        pedido.setIdCliente(cliente);
        pedido.setConfirmacionApartado(1);
        pedido.setConfirmacionVenta(0);
        pedido.setStatus(0);
        pedido.setTotal(dep);
        pedido.setSubtotal(subT);
        pedido.setFecha(hoy);
        pedidofac.create(pedido);
        for (libcant li : car.getCarro()) {
            detallepe = new Detallepedido();
            detallepe.setLibroIsbn(li.getLibro());
            detallepe.setCantidad(li.getCantidad());
            detallepe.setNumPedido(pedido);
            detallepe.setPrecio(li.getCantidad() * li.getLibro().getPrecio());
            detallepedidofac.create(detallepe);
        }
        return "confirmacion";
    }

    public String filtracat(int idcat) {
        libroList = new ArrayList();
        for (Libro lib : librofac.findAll()) {
            if (lib.getIdCategoria().getIdCategoria() == idcat) {
                libroList.add(lib);
            }
        }
        return "catego";
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getTarjeta() {
        return Tarjeta;
    }

    public void setTarjeta(String Tarjeta) {
        this.Tarjeta = Tarjeta;
    }

    public Date getFechaV() {
        return FechaV;
    }

    public void setFechaV(Date FechaV) {
        this.FechaV = FechaV;
    }

    public String comprobante() {
        pedido.setStatus(1);
        pedidofac.edit(pedido);
        return "comprobantePago";
    }

    public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
        Document pdf = (Document) document;
        pdf.open();
        pdf.setPageSize(PageSize.A4);

        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String logo = servletContext.getRealPath("") + File.separator + "resources" + File.separator + "img" + File.separator + "100.png";

        pdf.add(Image.getInstance(logo));
    }

}
