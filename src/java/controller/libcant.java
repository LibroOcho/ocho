package controller;

import entidad.Libro;

/**
 *
 * @author Daniel
 */
public class libcant {

    private Libro libro;
    private int Cantidad;
    private int id;

    public libcant(Libro libro, int Cantidad) {
        this.libro = libro;
        this.Cantidad = Cantidad;
    }

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
