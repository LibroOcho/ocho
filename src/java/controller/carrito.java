package controller;

import controller.libcant;
import entidad.Libro;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Daniel Olivares
 */
@ManagedBean
@SessionScoped
public class carrito {

    private List<libcant> carro = new ArrayList();
    public int cont = 0;
    private double subtotal;
    private double total;
    

    /**
     * Creates a new instance of carrito
     */
    public carrito() {
    }

    public String agregar(Libro lib, int cantidad) {
        libcant libo= new libcant(lib, cantidad);
        libo.setId(cont);
        carro.add(cont, libo);
        cont++;
        return "carro";
    }

    public String quitar(int cont) {
        carro.remove(cont);
        return "carro";
    }

    public void Limpiar() {
        carro = new ArrayList();
    }

    public List<libcant> getCarro() {
        return carro;
    }

    public void setCarro(List<libcant> carro) {
        this.carro = carro;
    }

    public double getSubtotal() {
        subtotal=0.0;
        for (libcant li : getCarro()) {
            subtotal += (li.getLibro().getPrecio()*li.getCantidad());
        }
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public double getTotal() {
        subtotal=0.0;
        for (libcant li : getCarro()) {
            subtotal += (li.getLibro().getPrecio()*li.getCantidad());
        }
        total=(1.16*subtotal);
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
     
}
