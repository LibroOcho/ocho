/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author DellPC
 */
@FacesValidator(value="tarjValidator")
public class tarjetaValidator implements Validator {
    
    private boolean checaTarj(String numTarj)
    {
        int suma=0;
        
            for (int i=numTarj.length() -1; i>=0;i-=2)
            {
                suma += Integer.parseInt((numTarj).substring(i,i+1));
                if(i>0)
                {
                    int d=2 * Integer.parseInt((numTarj).substring(i-1,i));                    
                    
                    if(d>9) d-=9;
                    suma +=d;
                }
            }
        return (suma%10==0);
       
    }
    

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
    //To change body of generated methods, choose Tools | Templates.
        String numTarj = String.valueOf(value);
    boolean valid = true;
    if (value  == null) {
      valid=false;  
    }else if (!checaTarj (numTarj)) {
        valid = false;
    }
    if (!valid){
        FacesMessage message= new FacesMessage (
            FacesMessage.SEVERITY_ERROR, "tarjeta no valida",
            "el numero de tarjeta no es valido");
        throw new ValidatorException(message);
    }
    }
    
        
    }
    

