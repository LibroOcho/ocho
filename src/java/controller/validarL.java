package controller;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("validarL")
public class validarL implements Validator{

    private static final String Nombre_valid = "^[a-zA-Z]+$";
    
    private final Pattern pattern;
    private Matcher matcher;
 
    
    public validarL(){
		  pattern = Pattern.compile(Nombre_valid);
	}
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String Nombre=(String)value;
        
        matcher = pattern.matcher(value.toString());
		if(!matcher.matches()){
 
			FacesMessage msg= new FacesMessage("El login no puede ser vacio");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
                }   
    }    
}
