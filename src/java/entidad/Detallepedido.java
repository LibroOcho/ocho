/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DellPC
 */
@Entity
@Table(name = "detallepedido")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detallepedido.findAll", query = "SELECT d FROM Detallepedido d"),
    @NamedQuery(name = "Detallepedido.findByIdDetallepedido", query = "SELECT d FROM Detallepedido d WHERE d.idDetallepedido = :idDetallepedido"),
    @NamedQuery(name = "Detallepedido.findByCantidad", query = "SELECT d FROM Detallepedido d WHERE d.cantidad = :cantidad"),
    @NamedQuery(name = "Detallepedido.findByPrecio", query = "SELECT d FROM Detallepedido d WHERE d.precio = :precio")})
public class Detallepedido implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_detallepedido")
    private Integer idDetallepedido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private int cantidad;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private BigDecimal precio;
    @JoinColumn(name = "num_pedido", referencedColumnName = "numero_pedido")
    @ManyToOne(optional = false)
    private Pedido numPedido;
    @JoinColumn(name = "libro_isbn", referencedColumnName = "isbn")
    @ManyToOne(optional = false)
    private Libro libroIsbn;

    public Detallepedido() {
    }

    public Detallepedido(Integer idDetallepedido) {
        this.idDetallepedido = idDetallepedido;
    }

    public Detallepedido(Integer idDetallepedido, int cantidad, BigDecimal precio) {
        this.idDetallepedido = idDetallepedido;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public Integer getIdDetallepedido() {
        return idDetallepedido;
    }

    public void setIdDetallepedido(Integer idDetallepedido) {
        this.idDetallepedido = idDetallepedido;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Pedido getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(Pedido numPedido) {
        this.numPedido = numPedido;
    }

    public Libro getLibroIsbn() {
        return libroIsbn;
    }

    public void setLibroIsbn(Libro libroIsbn) {
        this.libroIsbn = libroIsbn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDetallepedido != null ? idDetallepedido.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detallepedido)) {
            return false;
        }
        Detallepedido other = (Detallepedido) object;
        if ((this.idDetallepedido == null && other.idDetallepedido != null) || (this.idDetallepedido != null && !this.idDetallepedido.equals(other.idDetallepedido))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Detallepedido[ idDetallepedido=" + idDetallepedido + " ]";
    }
    
}
